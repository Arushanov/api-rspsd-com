<?php

namespace Rspsd;

use Silex;
use Symfony\Component\Debug\ErrorHandler;
use Symfony\Component\Form\Forms;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Debug\ExceptionHandler;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Silex\Provider\FormServiceProvider;

// So script can override current __DIR__ (used @ workers)
if (!defined('__ACTIVE_DIRECTORY__')) {
    define('__ACTIVE_DIRECTORY__', __DIR__);
}

$app =  Application::build();

$app['debug'] = true;

// Register Services
$app->register(new \Igorw\Silex\ConfigServiceProvider(__ACTIVE_DIRECTORY__ ."/config/config.php"));//Config

$bdconfig = $app['db'];

$app->register(new Silex\Provider\SessionServiceProvider());
$app->register(new Silex\Provider\SwiftmailerServiceProvider());
$app->register(new Silex\Provider\UrlGeneratorServiceProvider());
$app->register(new Silex\Provider\ValidatorServiceProvider());
$app->register(new FormServiceProvider());
$app->register(new Silex\Provider\ServiceControllerServiceProvider());
$app->register(new Silex\Provider\DoctrineServiceProvider(), array('db.options' => $bdconfig));
$app->register(new Silex\Provider\MonologServiceProvider(), array(
    'monolog.logfile' => __ACTIVE_DIRECTORY__.'/log/monolog.log',
));


return $app;
