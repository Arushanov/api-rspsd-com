<?php

namespace Rspsd\Controllers;


use Rspsd\Application;

abstract class Controller
{
    protected $_app;

    public function __construct()
    {
        $this->setApp(Application::build());
    }

    public function setApp(\Silex\Application $app)
    {
        $this->_app =& $app;
        return $this;
    }

    public function getApp()
    {
        return $this->_app;
    }
}
