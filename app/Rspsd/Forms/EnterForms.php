<?php

namespace Rspsd\Forms;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;


class EnterForms extends Form
{

    protected $form;

    public function __construct($app)
    {
        $this->buildForm($app);
    }

    private function buildForm($app)
    {
        $data = array(
            'name' => 'Your name',
            'email' => 'Your email',
        );

        $this->form = $app['form.factory']->createBuilder('form', $data)
            ->add('name')
            ->add('email')
            ->getForm();


    }

    public function getForm()
    {
      return $this->form;
    }
}

