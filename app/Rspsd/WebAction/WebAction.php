<?php

namespace Rspsd\WebAction;

use Rspsd\Application;


abstract class WebAction
{

    protected $_app;

    protected $_ctrl;


    public function __construct()
    {
        // Application
        $this->_app =& Application::build();

    }


    protected function _response($data, $twig, $folder = "default", $code = 200)
    {
        $twig = $folder . "/" . $twig;

        return $this->_app['twig']->render($twig, $data);
    }

}