<?php
namespace Rspsd\WebAction;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Rspsd\Forms;
use Rspsd\Forms\EnterForms;

class EnterWebAction extends WebAction
{
    public function index(){

        $app = $this->_app;


        $ctrl = new EnterForms($app);

        $form = $ctrl->getForm();

        $data['form'] = $form->createView();
        $data['name'] = "Dima";
        $data['title'] = "Title";

        return $this->_response($data,"enter.twig","enter");
    }
}

?>