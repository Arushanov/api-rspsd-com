<?php

namespace Rspsd;

class Application {

    protected static $app;

    public static function &build()
    {
        if (!self::$app)
            self::$app = new \Silex\Application();
        return self::$app;
    }
}