<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;

use Rspsd\Controllers;
use Rspsd\Controllers\TestController;

use Rspsd\WebAction;



// Twig
$app->register(new \Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/../web/views',
));

// Athlete WebAction
$app['enter.webaction'] = $app->share(function() use ($app) {
    return new WebAction\EnterWebAction();
});

// Ahtlete 2.0 Routes
$app->get('/', 'enter.webaction:index');

